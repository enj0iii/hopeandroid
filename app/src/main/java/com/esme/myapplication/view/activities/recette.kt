package com.esme.myapplication.view.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.ArrayAdapter
import android.widget.ListView
import com.esme.myapplication.R
import com.esme.myapplication.data.Dao.AppDatabase
import com.esme.myapplication.data.model.Ingredient

class recette : AppCompatActivity() {


    private lateinit var mlistview: ListView
    var mDb : AppDatabase? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recette)

       mDb = AppDatabase.getInMemoryDatabase(application)
       val ingre =  mDb?.ingredientModel()?.loadAllIngredients()

        val liste = getrecette(ingre as ArrayList<Ingredient>)

        mlistview = findViewById<ListView>(R.id.recette_list)

        Log.d("TOST","liste:"+liste.size+"ingre:"+ingre.size)

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, liste)

        mlistview.adapter = adapter








    }


    fun getrecette (ingredients: ArrayList<Ingredient>) : List<String>{

        val list = ArrayList<String>()

        //Omelette
        var size = mDb?.ingredientModel()?.findIngredientByNameAndLastName("oeuf")!!.size
        if(size > 0)
        {
            list.add("Omelette")
        }


        //oeuf mollet
        size = mDb?.ingredientModel()?.findIngredientByNameAndLastName("oeuf")!!.size
        if(size > 0)
        {
            list.add("oeuf mollet")
        }

         var size2 = (mDb?.ingredientModel()?.findIngredientByNameAndLastName("pain")!!.size) +
                 (mDb?.ingredientModel()?.findIngredientByNameAndLastName("fromage")!!.size) +
                 (mDb?.ingredientModel()?.findIngredientByNameAndLastName("jambon")!!.size)
        if(size2 > 2)
        {
            list.add("croque monsieur")
        }

        var coucou = (mDb?.ingredientModel()?.findIngredientByNameAndLastName("pain")!!.size) +
                (mDb?.ingredientModel()?.findIngredientByNameAndLastName("nutella")!!.size)

        if (coucou > 1)
        {
            list.add("tartine nutella")
        }



        return list
    }




    }











