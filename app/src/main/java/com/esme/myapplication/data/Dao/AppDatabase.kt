package com.esme.myapplication.data.Dao

import android.content.Context
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.util.Log

import com.esme.myapplication.data.model.Ingredient

@Database(entities = arrayOf(Ingredient::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun ingredientModel(): IngredientDao


    companion object {

        private var INSTANCE: AppDatabase? = null

        fun getInMemoryDatabase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java, "database-test")
                    .allowMainThreadQueries()
                    .build()
            }
            return INSTANCE as AppDatabase
        }

        fun destroyInstance() {
            INSTANCE = null
        }
    }
}