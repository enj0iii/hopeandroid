package com.esme.myapplication.view.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.esme.myapplication.R
import com.esme.myapplication.data.Dao.AppDatabase
import com.esme.myapplication.data.model.Ingredient
import kotlinx.android.synthetic.main.activity_ajouter_ingredient__bdd.*

class EnleverIngredient : AppCompatActivity() {

    private lateinit var mlistview: ListView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enlever_ingredient)


        mlistview = findViewById<ListView>(R.id.list_enlever_ingredient)





        val mDb = AppDatabase.getInMemoryDatabase(application)
        val list = mDb?.ingredientModel()?.loadAllIngredients()
        val listItems = arrayOfNulls<String>(list?.size!!)

        for (i in 0 until list.size) {
            val ingre = list[i]
            listItems[i] = ingre.mName
        }
        mlistview.setOnItemClickListener{
            parent: AdapterView<*>?, view: View?, position: Int, id: Long ->
            val bla = listItems[position]
            mDb?.ingredientModel()?.deleteIngredient(Ingredient(bla!!))
            finish()
            overridePendingTransition(0,0)
            startActivity(intent)
        }
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        mlistview.adapter = adapter


       }



       }




