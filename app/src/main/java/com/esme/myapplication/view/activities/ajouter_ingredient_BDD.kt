package com.esme.myapplication.view.activities

import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.esme.myapplication.R
import com.esme.myapplication.data.Dao.AppDatabase
import com.esme.myapplication.data.model.Ingredient
import kotlinx.android.synthetic.main.activity_ajouter_ingredient__bdd.*

class ajouter_ingredient_BDD : MainActivity() {
    private var mdb: AppDatabase? = null
    private lateinit var mlistview: ListView



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ajouter_ingredient__bdd)


        mlistview = findViewById(R.id.list_item_ajouter_ingre)

        val mDb = AppDatabase.getInMemoryDatabase(application)
        val list = mDb?.ingredientModel()?.loadAllIngredients()
        val listItems = arrayOfNulls<String>(list?.size!!)

        for (i in 0 until list.size) {
            val ingre = list[i]
            listItems[i] = ingre.mName
        }



       add_button.setOnClickListener {

           val mDb = AppDatabase.getInMemoryDatabase(application)
           mDb?.ingredientModel()?.insertIngredient(Ingredient(ingredient_name_edittext.text.toString()))
           Toast.makeText(applicationContext,ingredient_name_edittext.text.toString(),Toast.LENGTH_SHORT).show()
           finish()
           overridePendingTransition(0,0)
           startActivity(intent)


        }

        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listItems)
        mlistview.adapter = adapter
    }



}





