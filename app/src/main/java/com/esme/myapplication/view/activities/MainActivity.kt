package com.esme.myapplication.view.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.esme.myapplication.R
import com.esme.myapplication.data.Dao.AppDatabase
import com.esme.myapplication.data.model.Ingredient
import kotlinx.android.synthetic.main.activity_main.*

open class MainActivity : AppCompatActivity() {
    private var mdb: AppDatabase? = null
    val TAG = "MainActivity"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        button_base_de_donne.setOnClickListener {

            val intent : Intent = Intent(this, BddActivity::class.java)
            startActivity(intent)

        }


        button_frigo.setOnClickListener {

            val intent: Intent = Intent(this, recette::class.java)
            startActivity(intent)


        }

        button_envoye.setOnClickListener {

            val intent: Intent = Intent(this, Bluetooh2::class.java)
            startActivity(intent)

        }

        val mDb = AppDatabase.getInMemoryDatabase(application)

        var list = mDb?.ingredientModel()?.loadAllIngredients()

        Log.d("TEST",""+list?.size)

        mDb?.ingredientModel()?.insertIngredient(Ingredient("oeuf"))
        mDb?.ingredientModel()?.insertIngredient(Ingredient("toast"))
        mDb?.ingredientModel()?.insertIngredient(Ingredient("coucou"))
        mDb?.ingredientModel()?.insertIngredient(Ingredient("kebab"))
        mDb?.ingredientModel()?.insertIngredient(Ingredient("feta"))
        mDb?.ingredientModel()?.insertIngredient(Ingredient("bim"))

        list = mDb?.ingredientModel()?.loadAllIngredients()
        Log.d("TEST",""+list?.size)

    }
    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart()")
    }


    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume()")

    }


    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause()")

    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop()")

    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy()")

    }
}
